#include <iostream>
#include <stdlib.h> // atoi
// https://en.wikipedia.org/wiki/ANSI_escape_code
// https://stackoverflow.com/questions/15682537/ansi-color-specific-rgb-sequence-bash
// http://wiki.bash-hackers.org/scripting/terminalcodes

int main(int argc, char* argv[])
{
	int colorLength = 1;
	if(argc > 1)
	{
		colorLength = atoi(argv[1]);
	}

	char colorLength_spaces[2000000];
	for(int i = 0; i <= colorLength; i++)
	{
		if(i ==  colorLength)
		{
			colorLength_spaces[i] = '\0';
		}
		else
		{
			colorLength_spaces[i] = ' ';
		}
	}


	while(true)
	{
		for(int i = 16; i <= 231; i++)
		{
			std::cout << "\033[48;5;"<< i << "m" << colorLength_spaces << "\033[0;00m";
		}
	}
	return 0;
}
