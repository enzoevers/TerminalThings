PROGRAMS=Rainbow

.PHONY all

all: $(PROGRAMS)

$(PROGRAMS):
	g++ $@.cpp -o $@

clean:
	@rm -rf $(PROGRAMS)
